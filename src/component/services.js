import React from 'react';
import "../styles/services.scss";
function Services(props) {
    return (
        <div className="services">
            <div className="container">
                <div className="row">
                    <div className="col-md-12">
                        <h5>Our Services</h5>
                        <h2>Why People Choose Us</h2>
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-3">
                        <div className="card">
                            <div className="cardImg">
                               <div className="mask-image">

                               </div>
                            </div>
                            <div className="card-body">
                                <div className="title">
                                    Design
                                </div>
                                <p>a plan or drawing produced to<br/> show the look and function</p>
                                <div className="learnFlex">
                                    <div className="learnFlexContent">
                                        <div className="learn">
                                            Learn more
                                            <img src="images/Vector 3.svg" alt="no image"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-md-3">
                        <div className="card">
                           <div className="cardImg">
                               <div className="mask-image">

                               </div>
                           </div>
                            <div className="card-body">
                                <div className="title">
                                    Development
                                </div>
                                <p>Development is defined as the <br/> process of growth or new </p>
                                <div className="learnFlex">
                                    <div className="learnFlexContent">
                                        <div className="learn">
                                            Learn more
                                            <img src="images/Vector 3.svg" alt="no image"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-md-3">
                        <div className="card">
                            <div className="cardImg">
                                <div className="mask-image">

                                </div>
                            </div>
                            <div className="card-body">
                                <div className="title">
                                    Branding
                                </div>
                                <p>The marketing practice of <br/> creating a name, symbol or </p>
                                <div className="learnFlex">
                                    <div className="learnFlexContent">
                                        <div className="learn">
                                            Learn more
                                            <img src="images/Vector 3.svg" alt="no image"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-md-3">
                        <div className="card">
                            <div className="cardImg">
                               <div className="mask-image">

                               </div>
                            </div>
                            <div className="card-body">
                                <div className="title">
                                    Illustration
                                </div>
                                <p>An illustration is a decoration, <br/> interpretation or visual</p>
                                <div className="learnFlex">
                                    <div className="learnFlexContent">
                                        <div className="learn">
                                            Learn more
                                            <img src="images/Vector 3.svg" alt="no image"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Services;