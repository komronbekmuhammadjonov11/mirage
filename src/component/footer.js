import React from 'react';
import "../styles/footer.scss";
function Footer(props) {
    return (
        <div className="footer">
            <div className="backgroundTop">
                <div className="container">
                    <div className="row">
                        <div className="col-md-3">
                            <div className="logotip">
                                <img src="images/Mirage.svg" alt="no image"/>
                            </div>
                            <a href="#">560 N Riverview D Ste 658</a>
                            <a href="#">North Carolina DA 598745 USA</a>
                            <div className="connectionIcons">
                                <p>Follow us on</p>
                                <div className="iconsGroup">
                                    <div className="iconsGroupItem">
                                        <div className="iconsContent">

                                        </div>
                                    </div>
                                    <div className="iconsGroupItem">
                                        <div className="iconsContent">

                                        </div>
                                    </div>
                                    <div className="iconsGroupItem">
                                        <div className="iconsContent">

                                        </div>
                                    </div>
                                    <div className="iconsGroupItem">
                                        <div className="iconsContent">

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-md-3">
                            <h6>Resouces</h6>
                            <a href="#">Revisions</a>
                            <a href="#">Sheets</a>
                            <a href="#">Quiz</a>
                            <a href="#">Drill</a>
                        </div>
                        <div className="col-md-3">
                            <h6>About</h6>
                            <a href="#">About Us</a>
                            <a href="#">Partners</a>
                            <a href="#">Contact Us</a>
                            <a href="#">Careers</a>
                        </div>
                        <div className="col-md-3">
                            <h6>Helpful link</h6>
                            <a href="#">Mirage</a>
                            <a href="#">Solution</a>
                            <a href="#">Industries</a>
                            <a href="#">Services</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Footer;