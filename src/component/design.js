import React from 'react';
import "../styles/design.scss";

function Design(props) {
    return (
        <div className="design">
            <div className="bloobs">
                <img src="images/Bloobs (1).png" alt="no images"/>
            </div>
            <div className="container">
                <div className="row">
                    <div className="col-md-6">
                        <img src="images/pluto-artist-workspace.png" alt="no image"/>
                    </div>
                    <div className="col-md-6">
                        <h5>Design Things </h5>
                        <h2>We Loves To Design<br/>
                            Your Creative Ideas</h2>
                        <p>A creative concept is an overarching “Big Idea” that <br/>captures audience interest, influences
                            their emotional <br/>response and inspires them to take action.It is a unifying<br/> theme that can be
                            used across all campaign messages </p>
                        <button className="learnButton">
                            Learn more
                            <span className="buttonIcon">
                            <span className="arrow">
                                <img src="images/Vector 2 (1).svg" alt="no images"/>
                            </span>
                        </span>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Design;