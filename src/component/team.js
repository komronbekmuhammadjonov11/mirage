import React from 'react';
import "../styles/team.scss";

function Team(props) {
    return (
        <div className="team">
            <img src="images/Bloobs.png" className="bloobsImg" alt="no image"/>
            <div className="container">
                <div className="row">
                    <div className="col-md-6">
                        <h5>Our Teamwork </h5>
                        <h2>We Believe Success<br/>
                            Lies On Teamwork</h2>
                        <p>Good teamwork means a synergistic way of working with <br/> each person committed and working
                            towards a shared <br/> goal. ... It is therefore a necessity that leaders facilitate <br/> and build the
                            teamwork skills of their people if they are</p>
                        <button className="learnButton">
                            Learn more
                            <span className="buttonIcon">
                            <span className="arrow">
                                <img src="images/Vector 2 (1).svg" alt="no images"/>
                            </span>
                        </span>
                        </button>
                    </div>
                    <div className="col-md-6">
                        <img src="images/Illustration.png" alt="no image"/>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Team;